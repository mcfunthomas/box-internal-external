#!/usr/bin/python3
#!/usr/bin/env python

##############################################################################################################
### This SCRIPT computes dimentions for a Geometry Net of a BOX, based on its external dimentions provided ###
#####################                            DESIGN ver.II                            ####################
##############################################################################################################

import argparse

# positional args
parser = argparse.ArgumentParser(description='''
This SCRIPT computes dimensions for a Geometry Net of a BOX,
based on its external dimensions provided. 
Spaces between Net elements are 5mm.
dimensions in mm with NO DECIMAL places.''')
parser.add_argument('width', type=int, help='- the external width of the box in mm.')
parser.add_argument('depth', type=int, help='- the external depth of the box in mm.')
parser.add_argument('height', type=int, help='- the external height of the box in mm.')
parser.add_argument('thickness', type=int, help='- thickness of the material used to build the box in mm.')

args = parser.parse_args()
args.width = abs(args.width)
args.depth = abs(args.depth)
args.height = abs(args.height)
args.thickness = abs(args.thickness)

space = 5
x1, y1 = 0, 0 # TOP
x2, y2 = args.thickness, args.depth + space + args.height + space - args.thickness # BOTTOM
x3, y3 = 0, args.depth + space # FRONT
x4, y4 = args.width + space, args.depth + space # SIDE 1
x5, y5 = args.width + space + args.depth + space - 2*args.thickness, args.depth + space # BACK
x6, y6 = args.width + space + args.depth + space - 2*args.thickness + args.width + space, args.depth + space# SIDE 2

# creating the svg file
outside_box = "BoxExtDim-02_parse.svg"
f = open(outside_box, "w")
f.write(f'''<svg width="{x6+args.depth-2*args.thickness}" height="{y2+args.depth-2*args.thickness}">
    <rect
\tx="{x1}"
\ty="{y1}"
\twidth="{args.width}"
\theight="{args.depth}"
\tstyle="fill:#000000" />
    
    <rect
\tx="{x2}"
\ty="{y2}"
\twidth="{args.width-2*args.thickness}"
\theight="{args.depth-2*args.thickness}"
\tstyle="fill:#000000" />

    <rect
\tx="{x3}"
\ty="{y3}"
\twidth="{args.width}"
\theight="{args.height-args.thickness}"
\tstyle="fill:#000000" />

    <rect
\tx="{x4}"
\ty="{y4}"
\twidth="{args.depth-2*args.thickness}"
\theight="{args.height-args.thickness}"
\tstyle="fill:#000000" />

    <rect
\tx="{x5}"
\ty="{y5}"
\twidth="{args.width}"
\theight="{args.height-args.thickness}"
\tstyle="fill:#000000" />

    <rect
\tx="{x6}"
\ty="{y6}"
\twidth="{args.depth-2*args.thickness}"
\theight="{args.height-args.thickness}"
\tstyle="fill:#000000" />
</svg>''')
f.close()
print(f"The {outside_box} file has been generated.")
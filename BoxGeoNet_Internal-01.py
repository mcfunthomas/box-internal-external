#!/usr/bin/python3

##############################################################################################################
### This SCRIPT computes dimentions for a Geometry Net of a BOX, based on its internal dimentions provided ###
#####################                            DESIGN ver.I                             ####################
##############################################################################################################

F_Default = "\x1b[39m"
F_Red = "\x1b[31m"
F_Green = "\x1b[32m"

### collecting dimentions data ###
print(f"{F_Green}Input the internal dimensions of the box, A x B x H {F_Red}(negative values become positive){F_Default}.")
while True:
    width = input("Width (A): ")
    try:
        val = int(width)
        width = val
        break
    except ValueError:
        try:
            val = float(width)
            width = val
            break
        except ValueError:
            print(f"{F_Red}This is not a number. Please try again.{F_Default}")
while True:
    depth = input("Depth (B): ")
    try:
        val = int(depth)
        depth = val
        break
    except ValueError:
        try:
            val = float(depth)
            depth = val
            break
        except ValueError:
            print(f"{F_Red}This is not a number. Please try again.{F_Default}")
while True:
    height = input("height (H): ")
    try:
        val = int(height)
        height = val
        break
    except ValueError:
        try:
            val = float(height)
            height = val
            break
        except ValueError:
            print(f"{F_Red}This is not a number. Please try again.{F_Default}")
while True:
    thickness = input("Material thickness: ")
    try:
        val = int(thickness)
        thickness = val
        break
    except ValueError:
        try:
            val = float(thickness)
            thickness = val
            break
        except ValueError:
            print(f"{F_Red}This is not a number. Please try again.{F_Default}")

### OnScreen publishing the results ###
print()
print("Dimensions for cutting:")

A_in_TOP_BOTTOM = width + 2 * thickness
B_in_TOP_BOTTOM = depth + 2 * thickness
print(f"Top and Bottom (A * B): {F_Green}{A_in_TOP_BOTTOM} mm x {B_in_TOP_BOTTOM} mm{F_Default}.")

B_in_SIDES = depth
H_in_SIDES = height
print(f"Left and Right Sides (B * H): {F_Green}{B_in_SIDES} mm x {H_in_SIDES} mm{F_Default}.")

A_in_FRONT_BACK = width + 2 * thickness
H_in_FRONT_BACK = height
print(f"Front and Back (A x H): {F_Green}{A_in_FRONT_BACK} mm x {H_in_FRONT_BACK} mm{F_Default}.")
print()

### creating the svg file ###
space = 5
x1, y1 = 0, 0 # TOP
x2, y2 = 0, depth + 2 * thickness + space + height + space # BOTTOM
x3, y3 = 0, depth + 2 * thickness + space # FRONT
x4, y4 = width + 2 * thickness + space, depth + 2 * thickness + space # SIDE 1
x5, y5 = width + 2 * thickness + space + depth + space, depth + 2 * thickness + space # BACK
x6, y6 = width + 2 * thickness + space + depth + space + width + 2 * thickness + space, depth + 2 * thickness + space# SIDE 2

internal = "BoxGeoNet_Internal-01.svg"
f = open(internal, "w")
f.write(f'''<svg width="{x6 + depth}" height="{y2 + depth + 2 * thickness}">
    <rect
\tx="{x1}"
\ty="{y1}"
\twidth="{A_in_TOP_BOTTOM}"
\theight="{B_in_TOP_BOTTOM}"
\tstyle="fill:#000000" />

    <rect
\tx="{x2}"
\ty="{y2}"
\twidth="{A_in_TOP_BOTTOM}"
\theight="{B_in_TOP_BOTTOM}"
\tstyle="fill:#000000" />

    <rect
\tx="{x3}"
\ty="{y3}"
\twidth="{A_in_FRONT_BACK}"
\theight="{H_in_FRONT_BACK}"
\tstyle="fill:#000000" />

    <rect
\tx="{x4}"
\ty="{y4}"
\twidth="{B_in_SIDES}"
\theight="{H_in_SIDES}"
\tstyle="fill:#000000" />

    <rect
\tx="{x5}"
\ty="{y5}"
\twidth="{A_in_FRONT_BACK}"
\theight="{H_in_FRONT_BACK}"
\tstyle="fill:#000000" />

    <rect
\tx="{x6}"
\ty="{y6}"
\twidth="{B_in_SIDES}"
\theight="{H_in_SIDES}"
\tstyle="fill:#000000" />
</svg>''')
f.close()

print(f"The {F_Green}{internal}{F_Default} file has been generated.")